FROM python:3.9.5

WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt

EXPOSE 3000
CMD ["python", "/app/main.py"]
